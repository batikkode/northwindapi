<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Products extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);		
    }

    //Contoh HATEOAS pada resource products 
    public function index_get() {			
		$id = $this->get('id');	// deklarasi parameter id products	
		$products=[];  // array products
		try{	
			if ($id == '') {
				$data = $this->db->get('products')->result();
				foreach($data as $row=>$key):
					$products[]=["ProductID"=>$key->ProductID,
								 "ProductName"=>$key->ProductName,
								 "_links"=>[(object)["href"=>"suppliers/{$key->SupplierID}",
												"rel"=>"suppliers",
												"type"=>"GET"],
											(object)["href"=>"categories/{$key->CategoryID}",
												"rel"=>"categories",
												"type"=>"GET"]],																					
								"QuantityPerUnit"=>$key->QuantityPerUnit,
								"UnitPrice"=>$key->UnitPrice,
								"UnitsInStock"=>$key->UnitsInStock,
								"UnitsOnOrder"=>$key->UnitsOnOrder,
								"ReorderLevel"=>$key->ReorderLevel,	
								"Discontinued"=>$key->Discontinued	
								 ];
				endforeach;			
			} else {			
					$this->db->where('ProductID', $id);
					$data = $this->db->get('products')->result();				
					$products=["ProductID"=>$data[0]->ProductID,
									 "ProductName"=>$data[0]->ProductName,
									 "_links"=>[(object)["href"=>"suppliers/{$data[0]->SupplierID}",
													"rel"=>"suppliers",
													"type"=>"GET"],
												(object)["href"=>"categories/{$data[0]->CategoryID}",
													"rel"=>"categories",
													"type"=>"GET"]],																					
									"QuantityPerUnit"=>$data[0]->QuantityPerUnit,
									"UnitPrice"=>$data[0]->UnitPrice,
									"UnitsInStock"=>$data[0]->UnitsInStock,
									"UnitsOnOrder"=>$data[0]->UnitsOnOrder,
									"ReorderLevel"=>$data[0]->ReorderLevel,	
									"Discontinued"=>$data[0]->Discontinued	
									 ];						 				
			}
			$result = ["took"=>$_SERVER["REQUEST_TIME_FLOAT"],
					  "code"=>200,
					  "message"=>"Response successfully",
					  "data"=>$products];	
				$this->response($result, 200);	
		}catch (Exception $e){
			$result = ["took"=>$_SERVER["REQUEST_TIME_FLOAT"],
					  "code"=>401,
					  "message"=>"Access denied",
					  "data"=>null];	
			$this->response($result, 401);
		}								
    }
}
?>