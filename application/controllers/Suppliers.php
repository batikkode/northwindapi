<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Supplier extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);		     
    }
    
	//Contoh penerapan idempotency pada method post resource suppliers
	public function index_post() {
        $data = array(
                    'CompanyName'    => $this->post('CompanyName'),
                    'ContactName'   => $this->post('ContactName'),                   
					'ContactTitle'   => $this->post('ContactTitle'),  
					'Address'    => $this->post('Address'),
					'City'    => $this->post('City'),
					'Region'    => $this->post('Region'),
					'PostalCode'  => $this->post('PostalCode'),
					'Country'  => $this->post('Country'),
					'Phone'  => $this->post('Phone'),
					'Fax'  => $this->post('Fax'),
					'HomePage'  => $this->post('HomePage'));	
		$this->db->where("SupplierID",$this->post('SupplierID'));				
		$this->db->where("ContactName",$this->post('ContactName'));
		$check = $this->db->get("suppliers")->num_rows();
		if($check==0):
			$insert = $this->db->insert('suppliers', $data);
			if ($insert) {            
				$result = ["took"=>$_SERVER["REQUEST_TIME_FLOAT"],
							"code"=>201,
							"message"=>"Data has successfully added",
							"data"=>$data];	
				$this->response($result, 201);
			} else {
				$result = ["took"=>$_SERVER["REQUEST_TIME_FLOAT"],
					  "code"=>502,
					  "message"=>"Failed adding data",
					  "data"=>null];	
				$this->response($result, 502);            
			}
		else:
			$result = ["took"=>$_SERVER["REQUEST_TIME_FLOAT"],
					  "code"=>304,
					  "message"=>"Data already added.",
					  "data"=>null];	
				$this->response($result, 304); 
		endif;	
    }
	
	
    
}
?>